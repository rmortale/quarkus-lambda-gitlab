#!/usr/bin/env bash
 
echo "Downloading GraalVM"
curl -L https://github.com/graalvm/graalvm-ce-builds/releases/download/vm-20.1.0/graalvm-ce-java11-linux-amd64-20.1.0.tar.gz > graalvm-ce-java11-linux-amd64-20.1.0.tar.gz
tar -zxf graalvm-ce-java11-linux-amd64-20.1.0.tar.gz -C ${CI_PROJECT_DIR}/
ls -la
 
echo "Installing GraalVM via gu"
 
${CI_PROJECT_DIR}/graalvm-ce-java11-20.1.0/bin/gu install native-image
